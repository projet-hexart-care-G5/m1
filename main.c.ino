#include "cardio.h"
#include "cardio.c"
#include <Arduino.h>

int valeurPrecedente = 0;
long tempsPrecedent = 0;
long tempsDetection;

void setup() {
  Serial.begin(9600);
}


void loop() {

  int valeurActuelle, valeurSeuil;

  valeurActuelle = analogRead(0);
  valeurSeuil = 390;

  if (valeurActuelle > valeurSeuil) {  // on est dans la zone max
    if (valeurPrecedente <= valeurSeuil) {  //
      tempsDetection = temps();
      if (tempsDetection > (tempsPrecedent + 200 )){  //
        Serial.print(tempsDetection );
        Serial.print(";");
        Serial.println((1000.0 * 60.0) / (tempsDetection - tempsPrecedent),0);
        tempsPrecedent = tempsDetection;
     
      }
    }
  }

  valeurPrecedente = valeurActuelle;

}
